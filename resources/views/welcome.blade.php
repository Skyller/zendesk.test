<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <!-- Start of hierohelp Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=49429274-925c-4318-a821-e82bee813a6f"> </script>
<!-- End of hierohelp Zendesk Widget script -->
<script src="https://me.botxo.co/sdk/webchat.js">
</script>
<script>
  initBotXOChat({
    id: "1047a9bc-1fb3-4ace-8f8d-89ec2d44496d",
    buttonColor: "#2ab4c0"
  });
</script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hiero-zenzen</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #d5f2ee;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 23px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="content">
                <div class="title m-b-md">
                    <b>Hiero Zendesk</b>
                    <sub><sup><small>(Simple demo)</small></sup></sub><br>
                </div>
                <div class="links">
                    <a href="">Company profile</a>
                    <a href="">Profile detail</a>
                    <a href="">About Hiero-zenzen</a>
                </div><br>
                <h3>Please ignore about this content<small> (Tongue twisters)</small></h3>
                    To sit in solemn silence in a dull, dark dock, <br>
                    In a pestilential prison, with a life-long lock, <br>
                    Awaiting the sensation of a short, sharp shock, <br>
                    From a cheap and chippy chopper on a big black block! <br>
                    To sit in solemn silence in a dull, dark dock, <br>
                    In a pestilential prison, with a life-long lock, <br>
                    Awaiting the sensation of a short, sharp shock, <br>
                    From a cheap and chippy chopper on a big black block! <br>
                    A dull, dark dock, a life-long lock, <br>
                    A short, sharp shock, a big black block! <br>
                    To sit in solemn silence in a pestilential prison, <br>
                    And awaiting the sensation <br>
                    From a cheap and chippy chopper on a big black block!

            </div>
            <h5>Five Question can ask the bot:<br>
            What is dmk status now?<br>
            Who are you?<br>
            What are you?<br>
            How much is DMK?<br>
            Where are you from?<br><br>
            Contact us to learn about tongue twisters<br>
            Follow our Youtube channel "Hiero ZenZen"<br>
            <strong><b>Email : piupiu@gmail.com</b></strong><br>
            <strong><b>Password : User@1234</b></strong><h5>
        </div>
    </body>
</html>