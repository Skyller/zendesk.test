<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', array('uses' => 'HomeController@showLogin'));
Auth::routes();
// route to show the login form

Route::get('logout', array('uses' => 'HomeController@doLogout'));
// app/routes.php


Route::get('/home', 'HomeController@index')->name('home');
